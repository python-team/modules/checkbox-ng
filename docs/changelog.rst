ChangeLog
=========

.. note::
    This changelog contains only a summary of changes. For a more accurate
    accounting of development history please inspect the source history
    directly.

.. _version_0_23:

CheckboxNG 0.23 (unreleased)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Bugfixes: https://launchpad.net/checkbox-ng/+milestone/0.23

.. _version_0_22:

CheckboxNG 0.22
^^^^^^^^^^^^^^^

* Bugfixes: https://launchpad.net/checkbox-ng/+milestone/0.22

CheckboxNG 0.3
^^^^^^^^^^^^^^

* Bugfixes: https://launchpad.net/checkbox-ng/+milestone/0.3

CheckboxNG 0.2
^^^^^^^^^^^^^^

* Bugfixes: https://launchpad.net/checkbox-ng/+milestone/0.2

CheckboxNG 0.1
^^^^^^^^^^^^^^

* Initial release
* Support for displaying configuration
* Support for running SRU tests (automatic regression testing)
