Source: checkbox-ng
Section: utils
Priority: optional
Maintainer: Checkbox Developers <checkbox-devel@lists.ubuntu.com>
Uploaders: Sylvain Pineau <sylvain.pineau@canonical.com>,
           Zygmunt Krynicki <zygmunt.krynicki@canonical.com>,
           Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python3-all,
               python3-checkbox-support (>= 0.22),
               python3-docutils,
               python3-guacamole (>= 0.9),
               python3-plainbox (>= 0.24),
               python3-requests (>= 1.0),
               python3-setuptools,
               python3-sphinx,
               python3-xlsxwriter
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/checkbox-ng.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/checkbox-ng
Homepage: https://launchpad.net/checkbox

Package: checkbox-ng
Architecture: all
Depends: python3-checkbox-ng (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends}
Replaces: checkbox (<< 0.17.6-0ubuntu1), python3-checkbox-ng (<< 0.23-1)
Breaks: checkbox (<< 0.17.6-0ubuntu1), python3-checkbox-ng (<< 0.23-1)
Description: PlainBox based test runner
 CheckBoxNG is a hardware testing tool useful for certifying laptops,
 desktops and servers. It is a new version of CheckBox that is built
 directly on top of PlainBox. CheckBoxNG replaces CheckBox, where applicable.
 .
 This package contains the CheckBox command line tool

Package: python3-checkbox-ng
Architecture: all
Depends: python3-checkbox-support (>= 0.22),
         python3-guacamole (>= 0.9),
         python3-plainbox (>= 0.24),
         python3-requests (>= 1.0),
         python3-xlsxwriter,
         ${misc:Depends},
         ${python3:Depends}
Description: PlainBox based test runner (Python 3 library)
 CheckBoxNG is a hardware testing tool useful for certifying laptops,
 desktops and servers. It is a new version of CheckBox that is built
 directly on top of PlainBox. CheckBoxNG replaces CheckBox, where applicable.
 .
 This package contains the checkbox-ng Python 3 library

Package: python3-checkbox-ng-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: PlainBox based test runner (documentation)
 CheckBoxNG is a hardware testing tool useful for certifying laptops,
 desktops and servers. It is a new version of CheckBox that is built
 directly on top of PlainBox. CheckBoxNG replaces CheckBox, where applicable.
 .
 This package contains the documentation for the checkbox-ng Python 3 library
